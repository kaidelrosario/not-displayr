import csv from 'csv';
import {useDropzone} from 'react-dropzone';
import styles from '@/styles/fileuploader.module.css'
import Button from './Button';

export default function FileUploader({
  handleUpload
}:{
  handleUpload(text: string):void
}) {

  const {
    acceptedFiles,
    fileRejections,
    getRootProps,
    open,
    getInputProps
  } = useDropzone({
    accept: {
      'text/csv': [],
    },
    maxSize: 3000000,
    minSize: 0,
    maxFiles: 1,
    onDrop: acceptedFiles => {
      acceptedFiles.forEach(file => {
        const reader = new FileReader();
        reader.onload = () => {
          if(typeof reader.result === 'string') {
            handleUpload(reader.result)
          }
        };
        reader.readAsText(file)
      })
    }
  });

  const acceptedFileItems = acceptedFiles.map((file:any) => (
    <li key={file.path}>
      {file.path} - {file.size} bytes
    </li>
  ));

  const fileRejectionItems = fileRejections.map(({ file, errors }:{file: any, errors:any}) => (
    <ul className={styles.errors}>
        {errors.map((e:any) => (
          <li key={e.code} className={styles.error}>{e.message}</li>
        ))}
    </ul>
  ));
  return(
    <>
      <section className={styles.container}>
        <div {...getRootProps({className: 'dropzone'})}>
          <div className={styles.dropzone}>
            <Button 
              label='Upload File'
              onClickFunction={open}
            />
            <input {...getInputProps()} />
            <p className={styles.description}>or drop your file here</p>
            <p className={styles.info}>{`Supported file format (.csv) - Max file size 3mb`}</p>
            {fileRejectionItems}
          </div>
        </div>
      </section>
    </>
  )
}