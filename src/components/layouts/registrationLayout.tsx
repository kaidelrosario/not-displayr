'use client'
import styles from "../../styles/registration.module.css";
import logo from '../../assets/logo.svg'
import type Link from "@/types/link";
import { UserProvider } from "@/contexts/user-context";

export default function RegistrationLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  const linkArray:Array<Link> = [
    {
      name: 'Terms of Use',
      url: '/terms-of-use'
    },
    {
      name: 'Privacy',
      url: '/privacy-policy'
    },
    {
      name: 'Contact Us',
      url: '/contact-us'
    }
  ]

  const links = linkArray.map((link: Link) => {
    return <li key={link.name}>
      <a href={link.url}>{link.name}</a>
    </li>
  })

  return (
    <>
      <div className={styles.login}>
        <header className={styles.header}>
          <div className="logo">
            <a href="/">
              <img src={logo.src} height="22px" alt="logo" />
            </a>
          </div>
        </header>
        <div className={styles.body}>
          {children}
        </div>
        <footer className={styles.footer}>
          <ul>
            {links}
          </ul>
        </footer>
      </div>
    </>
  );
}
