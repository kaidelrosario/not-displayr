import styles from '@/styles/integration.module.css'

export default function Integration({
  img,
  alt,
  label,
  onClickFunction
}: {
  img: string,
  alt: string,
  label: string,
  onClickFunction?: Function
}) {

  const clickHandler = () => {
    if (onClickFunction) {
      onClickFunction() 
    }
  }

  return (
    <a className={styles.integration} onClick={clickHandler}>
      <img src={img} alt={alt}/>
      <span>{label}</span>
    </a>
  )
}