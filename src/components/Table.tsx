import styles from '@/styles/table.module.css'

export default function Table({
  data
}: {
  data: Array<Object>
}) {
  const tableHeaders = Object.keys(data[0]).map(name => {
    return <th>{name}</th>
  })
  const tableRows = data.map(datum => {
    const cells = Object.values(datum).map(value => {
      return <td>{value}</td>
    })
    return <tr className={styles.tableRow}>{cells}</tr>
  })
  return (
    <table className={styles.table}>
      <tr className={styles.tableHeader}>
        {tableHeaders}
      </tr>
      {tableRows}
    </table>
  )
}