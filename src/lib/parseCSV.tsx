export default function parseCSV(text:string):Array<Object> {
  const lines = text.split('\n');
  const headers = lines[0].split(',');
  const rows = lines.slice(1).map(line => {
    const values = line.split(',');
    return headers.reduce((obj:any, header, index) => {
      obj[header] = values[index];
      return obj;
    }, {});
  });
  return rows;
};