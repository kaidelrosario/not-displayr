'use client'
import React, {useContext} from "react";
import styles from '@/styles/usermenu.module.css'
import { logout } from '@/lib/firebase/firebase-functions'
import UserContext from "@/contexts/user-context";
import { useRouter } from 'next/navigation'

export default function UserMenu() {
  const {user, setUser} = useContext(UserContext);
  const router = useRouter()

  const signout = async () => {
    try {
      await logout()
      setUser(null);
      router.push('/login')
      localStorage.removeItem('user')
    } catch (error) {
      console.error('Could not logout')
    }
  }

  return (
    <div className={styles.userMenu}>
      <div className={styles.userContainer}>
        {/* would be better if it were name and email but we don't have that in our model atm */}
        <div className={styles.userInfo}>{user?.email}</div>
        <div className={styles.userDescription}>{user?.email}</div>
      </div>
      <div className={styles.menuActions}>
        <ul className={styles.menuActionsList}>
          <li>
            {/* I would make this more dynamic, but for now the only purpose this serves is to log out. */}
            <a className={styles.menuLink} onClick={signout}>Sign out</a>
          </li>
        </ul>
      </div>
    </div>
  )
}