export default function scriptLoader(src: string, onload: () => void) {
  const script = document.createElement('script');
  script.src = src;
  script.onload = onload;
  document.body.appendChild(script);
}