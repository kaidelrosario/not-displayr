'use client'
import React, {useState, useContext} from "react";
import { useRouter } from 'next/navigation'
import styles from "../../styles/registration.module.css";
import TextBox from "@/components/TextBox";
import Button from "@/components/Button";
import { isEmailValid } from "@/lib/isEmailValid";
import _ from "lodash";
import { signInEmail } from "@/lib/firebase/firebase-functions";
import RegistrationLayout from '../../components/layouts/registrationLayout'
import UserContext from "@/contexts/user-context";

export default function Login () {
  const [emailValue, setEmailValue] = useState<string>("");
  const [emailTouched, setEmailTouched] = useState<boolean>(false);
  const [passwordValue, setPasswordValue] = useState<string>("");
  const [loginErrorValue, setLoginErrorValue] = useState<string | null>(null);
  const [emailErrorValue, setEmailErrorValue] = useState<string | null>(null);
  const [passwordErrorValue, setPasswordErrorValue] = useState<string | null>(null);
  const [buttonDisabled, setButtonDisabled] = useState<boolean>(false)
  const {user, setUser} = useContext(UserContext);
  const router = useRouter()

  const signIn = async() => {
    const emailValid = isEmailValid(emailValue)
    const passwordValid = passwordValue.length > 0
    if (emailValid && passwordValid) {
      setButtonDisabled(true)
      setPasswordErrorValue(null);
      try {
        const user = await signInEmail(emailValue, passwordValue);
        setButtonDisabled(false)
        setUser(user)
        router.push('/')
        const userJSON = JSON.stringify(user)
        localStorage.setItem("user", userJSON);
      } catch (error) {
        setLoginErrorValue('The email or password is incorrect. Please try again.')
        setPasswordValue("")
        setButtonDisabled(false)
      }
    } else {
      if(!emailValid) {
        setEmailErrorValue('Please enter a valid email address.')
      }
      if(!passwordValid) {
        setPasswordErrorValue('Please enter your password.');
      }
    }
  }

  const validateEmail = (email: string) => {
    const isValid = isEmailValid(email)
    if(!isValid) {
      setEmailErrorValue('Please enter a valid email address.')
    } else {
      setEmailErrorValue(null)
    }
  }

  const debounce = _.debounce(() => validateEmail(emailValue), 500);

  const changeEmail = (e: React.ChangeEvent<HTMLInputElement>) => {
    const email = e.target.value;
    setEmailValue(email);
    if(!emailTouched) {
      setEmailTouched(true);
    }
    
    if(emailTouched) {
      debounce();
    }
  }

  const changePassword = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPasswordValue(e.target.value);
    setPasswordErrorValue(null);
  }

  return (
    <RegistrationLayout>
      <div className={styles.narrowContainer}>
        <div className={styles.title}>
        <h1 className={styles.title}>Log In</h1>
          {
            loginErrorValue &&
            <p className={styles.error}>
              {loginErrorValue}
            </p>
          }
        </div>
        <form id="loginForm" className={styles.form}>
          <TextBox
            id="txtUserID"
            label="Email"
            type="email"
            value={emailValue}
            onChangeText={changeEmail}
            error={emailErrorValue}
          />
          <TextBox
            id="txtUserPassword"
            label="Password"
            type="password"
            value={passwordValue}
            onChangeText={changePassword}
            error={passwordErrorValue}
            link={  
              {
                name: "Forgot Password?",
                url: "/forgot-password"
              }
            }
          />
          <Button
            label="Log In"
            onClickFunction={signIn}
            disabled={buttonDisabled}
          />
        </form>
        <div className={styles.extra}>
          <span>Don't have an account? </span>
          <a href="/signup">Sign up</a>
        </div>
      </div>
    </RegistrationLayout>
  )
}