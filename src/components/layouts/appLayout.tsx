'use client'
import React, { useState, useContext } from 'react';
import logo from '../../assets/logo.svg'
import styles from '../../styles/app.module.css'
import Avatar from '../Avatar';
import UserMenu from '../UserMenu';
import UserContext from "@/contexts/user-context";


export default function AppLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  const [menuToggled, setMenuToggled] = useState<boolean>(false)
  const {user} = useContext(UserContext);

  const toggleMenu = () => {
    setMenuToggled(!menuToggled)
  }
  return(
    <>
      <header className={styles.header}>
        <a href="/">
          <img src={logo.src} height="22px" alt="logo" />
        </a>
        {user && <Avatar user={user} onClickFunction={toggleMenu} />}
        {menuToggled && <UserMenu/>}
      </header>
      <main className={styles.main}>
        {children}
      </main>
    </>
  )
}