
# Hello!

 So, this is just a little test app that aims to generate a table from various data sources. 

 As this was developed over the span of 3-4 days, there are some limitations which will be detailed below.
 



## Dependencies
1. React
2. Next
3. Firebase
4. Google Cloud Console


## Installation

1. First run `npm install` to install.
2. Then, go to firebase console and make your own firebase app. Get the values and paste them into an `.env` file. I provided a `.env.example` file to use as a base.
3. Set up a google cloud console project with read access permissions for google sheets. Create credentials and an API key. Fill out the values in the `.env` file.
4. Afterwards, just run `npm run dev`.
    
## Limitations

#### React and NextJS

I haven't used React in a while, and I wanted to play around/study it. I was looking at various frameworks on the react.dev website, and saw they recommended NextJS. I should really study React on its own without using NextJs.

#### Routing & Auth

When it comes to routing/redirection, I did a 'hacky' way where I redirect the user from the App itself if they are not logged in. Honestly, I would prefer to use a middleware, but due to time constaints, I chose this method. I'm used to Nuxt middleware where it can be done from the clientside (while from reading the docs my understanding is NextJS middleware routes from the server). So I definitely need to find a way to do this more elegantly.

#### Google Sheets

Currently this only gets the sheets that come with the example. With more work/tinkering (and because it has permissions needed to read sheets from the user's account), you can get the user's sheets.

#### Airtable

I wanted to do the entire integration (with OAUth), but I kept getting blocked by CORS. Will need to do more research. For now, I'm just using a personal API key + one of my personal bases to get data.