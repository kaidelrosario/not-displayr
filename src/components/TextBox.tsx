import styles from "../styles/inputs.module.css"
import Link from "@/types/link"

export default function TextBox(
  {
    id,
    label,
    value,
    onChangeText,
    placeholder,
    type = "text",
    autocomplete = "off",
    required = false,
    link,
    error
  }: {
    id: string,
    label: string,
    value: string,
    onChangeText(e: React.ChangeEvent<HTMLInputElement>):void,
    placeholder?:string,
    type?: "text" | "password" | "email",
    autocomplete?: "on" | "off",
    required?: boolean,
    link?: Link,
    error?: string | null
  }
) {
  return(
    <div className={styles.textbox}>
      <label htmlFor={id}>{label}</label>
      <input
        type={type} 
        id={id} 
        name={id} 
        value={value}
        onChange={onChangeText}
        placeholder={placeholder}
        autoComplete={autocomplete}
        required={required}
        className={`${styles.input} ${value !== "" ? styles.filled : ""} ${error ? styles.inputError : ""}`}
      />
      {
        link &&
        <a href={link.url} className={styles.textboxLink}>{link.name}</a>
      }
      {error && <span className={styles.errorMessage}>{error}</span>}
    </div>
  )
}