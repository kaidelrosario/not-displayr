import styles from "../styles/buttons.module.css"

export default function Button({
  label,
  onClickFunction,
  disabled = false
}: {
  label: string,
  onClickFunction:Function,
  disabled?: boolean
}) {
  const onClickHandler = (e: React.MouseEvent<HTMLElement>) => {
    e.stopPropagation();
    e.preventDefault();
    onClickFunction()
  }
  return (
    <button className={styles.button} onClick={onClickHandler} disabled={disabled}>{label}</button>
  )
}