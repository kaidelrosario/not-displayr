'use client'
import React, {useState, useContext} from "react";
import { useRouter } from 'next/navigation'
import styles from "../../styles/registration.module.css";
import TextBox from "@/components/TextBox";
import Button from "@/components/Button";
import { isEmailValid } from "@/lib/isEmailValid";
import _ from "lodash";
import { createUser } from "@/lib/firebase/firebase-functions";
import RegistrationLayout from '../../components/layouts/registrationLayout'
import UserContext from "@/contexts/user-context";

export default function Login () {
  const [emailValue, setEmailValue] = useState<string>("");
  const [emailTouched, setEmailTouched] = useState<boolean>(false);
  const [passwordValue, setPasswordValue] = useState<string>("");
  const [emailValid, setEmailValid] = useState<boolean>(false)
  const [passwordValid, setPasswordValid] = useState<boolean>(false)
  const [buttonDisabled, setButtonDisabled] = useState<boolean>(false)
  const [signupErrorValue, setSignupErrorValue] = useState<string | null>(null);
  const [emailErrorValue, setEmailErrorValue] = useState<string | null>(null);
  const [passwordErrorValue, setPasswordErrorValue] = useState<string | null>(null);
  const {user, setUser} = useContext(UserContext);
  const router = useRouter()

  const signUp = async() => {
    if (emailValid && passwordValid) {
      setButtonDisabled(true)
      try {
        const user = await createUser(emailValue, passwordValue);
        setButtonDisabled(false)
        router.push('/')
        const userJSON = JSON.stringify(user)
        localStorage.setItem("user", userJSON);
      } catch (error) {
        setSignupErrorValue('There was a problem with signing up. Please try again.')
        setButtonDisabled(false)
      }
    } else {
      if(!emailValid) {
        setEmailErrorValue('Please enter a valid email address.')
      }

      if(!passwordValid) {
        setPasswordErrorValue('Please enter a password greater than 8 characters.')
      }

    }
  }

  const validateEmail = (email: string) => {
    const isValid = isEmailValid(email)
    if(!isValid) {
      setEmailErrorValue('Please enter a valid email address.')
      setEmailValid(false)
    } else {
      setEmailErrorValue(null)
      setEmailValid(true)
    }
  }

  const validatePassword = (password: string) => {
    const isValid = password.length > 8
    if(!isValid) {
      setPasswordErrorValue('Please enter a password greater than 8 characters.')
      setPasswordValid(false)
    } else {
      setPasswordErrorValue(null)
      setPasswordValid(true)
    }
  }

  const debounceEmail = _.debounce(() => validateEmail(emailValue), 500);
  const debouncePassword = _.debounce(() => validatePassword(passwordValue), 500);

  const changeEmail = (e: React.ChangeEvent<HTMLInputElement>) => {
    const email = e.target.value;
    setEmailValue(email);
    if(!emailTouched) {
      setEmailTouched(true);
    }
    
    if(emailTouched) {
      debounceEmail();
    }
  }

  const changePassword = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPasswordValue(e.target.value);
    debouncePassword()
  }

  return (
    <RegistrationLayout>
      <div className={styles.narrowContainer}>
        <div className={styles.title}>
        <h1 className={styles.title}>Create your free account</h1>
          <h2>Unlimited features for 14 days</h2>
          {
            signupErrorValue &&
            <p className={styles.error}>
              {signupErrorValue}
            </p>
          }
        </div>
        <form id="loginForm" className={styles.form}>
          <TextBox
            id="txtUserID"
            label="Email"
            type="email"
            value={emailValue}
            onChangeText={changeEmail}
            error={emailErrorValue}
          />
          <TextBox
            id="txtUserPassword"
            label="Password"
            type="password"
            value={passwordValue}
            onChangeText={changePassword}
            error={passwordErrorValue}
          />
          <Button
            label="Get Started"
            disabled={buttonDisabled}
            onClickFunction={signUp}
          />
        </form>
        <div className={styles.extra}>
          <span>Already have an account? </span>
          <a href="/login">Log in</a>
        </div>
      </div>
    </RegistrationLayout>
  )
}