export const clientCredentials = {
  "web": {
    "client_id":process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID,
    "project_id":process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID_PROJECT_ID,
    "auth_uri":process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID_AUTH_URI,
    "token_uri":process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID_TOKEN_URI,
    "auth_provider_x509_cert_url":process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID_AUTH_PROVIDER,
    "client_secret":process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID_CLIENT_SECRET,
    "redirect_uris":["http://localhost:3000"],
    "javascript_origins":["http://localhost:3000"]
  }
}