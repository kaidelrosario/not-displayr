'use client'
import React, { useState, useEffect, useContext } from "react";
import styles from "./page.module.css";
import AppLayout from "@/components/layouts/appLayout";
import FileUploader from "@/components/FileUploader";
import parseCSV from "@/lib/parseCSV";
import Table from "@/components/Table";
import { authListener } from "@/lib/firebase/firebase-functions";
import UserContext from "@/contexts/user-context";
import { useRouter } from 'next/navigation'
import GoogleSheets from "@/components/GoogleSheets";
import createDataFromGoogleSheets from "@/lib/createDataFromGoogleSheets"
import AirTable from "@/components/AirTable";

export default function Home() {
  const [data, setData] = useState<Array<any>>([])
  const {user, setUser} = useContext(UserContext);
  const [errorMessage, setErrorMessage] = useState<String | null>(null)
  const router = useRouter()

  const handleCSV = (text: string) => {
    setData(parseCSV(text))
  }

  const handleGoogleSheets = (text: string) => {
    // do something here
    const sheets = JSON.parse(text)
    const sheetsData = createDataFromGoogleSheets(sheets.values);
    setData(sheetsData)
  }

  const handleAirTable = (records: Array<Object>) => {
    setData(records)
  }

  const handleIntegrationError = (error: string) => {
    setErrorMessage(error)
  }

  // I would use middleware for this
  // This is just a demonstration, I wouldn't normally do it like this haha
  if (!user) {
    router.push('/login')
  }

  useEffect(() => {
    const unsubscribe = authListener(setUser)
    return () => unsubscribe();
  }, []);


  return (
    <AppLayout>
      { data.length === 0 ?
        <div className={styles.tableSource}>
          <h1>Where is your data?</h1>
          <FileUploader handleUpload={handleCSV} />
          <h2>Or choose an integration</h2>
          <ul className={styles.integrationsSelector}>
            <li>
              <GoogleSheets
                handleError={handleIntegrationError}
                handleSheets={handleGoogleSheets}
              />
            </li>
            <li>
              <AirTable
                handleData={handleAirTable}
                handleError={handleIntegrationError}
              />
            </li>
          </ul>
          {
            errorMessage &&
            <div className={styles.errorContainer}>
              {errorMessage}
            </div>
          }
        </div>
        :
        <div className={styles.dataSource}>
          <Table data={data}/>
        </div>
      }
    </AppLayout>
  );
}
