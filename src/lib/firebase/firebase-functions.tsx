import {
  getAuth,
  signInWithEmailAndPassword,
  signOut,
  sendPasswordResetEmail,
  createUserWithEmailAndPassword,
  onAuthStateChanged
} from "firebase/auth";
import app from "./firebase-config";
import type {User} from '../../types/user'

const auth = getAuth(app);

export async function signInEmail(email: string, password: string):Promise<User> {
  const userCredential = await signInWithEmailAndPassword(auth, email, password);
  const user:User = {
    userId: userCredential.user.uid,
    email: userCredential.user.email,
  }
  return user
}

export async function logout():Promise<void> {
  await signOut(auth)
}

export async function resetEmail(email: string):Promise<void> {
  await sendPasswordResetEmail(auth,email)
}

export async function createUser(email: string, password: string):Promise<User> {
  const userCredential = await createUserWithEmailAndPassword(auth, email, password)
  const user:User = {
    userId: userCredential.user.uid,
    email: userCredential.user.email,
  }
  return user
}

export function authListener (setUser: Function):Function {
  return onAuthStateChanged(auth, (userAuth) => {
    if (userAuth) {
      const user:User = {
        userId: userAuth.uid,
        email: userAuth.email,
      }
      setUser(user)
    } else {
      setUser(null)
    }
  });
}