export interface AirtableResult {
  commentCount?: number,
  fields: Object,
  id: string, 
}