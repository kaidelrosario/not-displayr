'use client'
import { createContext, useState } from "react";
import { User } from "@/types/user";

const UserContext = createContext<{user: User | null, setUser: Function}>({user: null, setUser: () => null});

export function UserProvider({
  children
}: {
  children: React.ReactNode
}) {
  const localUser = localStorage.getItem('user')
  const parsedUser:User | null = localUser ? JSON.parse(localUser) : null
  const [user, setUser] = useState<User | null>(parsedUser)
  return (
    <UserContext.Provider value={{user: user, setUser: setUser}}>
      {children}
    </UserContext.Provider>
  )
}

export default UserContext;