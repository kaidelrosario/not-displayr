'client only'
import React, { useEffect, useState } from 'react';
import { gapi } from 'google-api-javascript-client';
import scriptLoader from '@/lib/scriptLoader';
import Integration from './integration';
import googleSheets from "@/assets/googlesheets.png"

const CLIENT_ID = process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID;
const API_KEY = process.env.NEXT_PUBLIC_GOOGLE_API_KEY
const DISCOVERY_DOC = 'https://sheets.googleapis.com/$discovery/rest?version=v4';
const SCOPES = 'https://www.googleapis.com/auth/spreadsheets.readonly';

declare global {
  interface Window {
    google: any;
  }
}

export default function GoogleSheets (
  {
    handleSheets,
    handleError
  }: {
    handleSheets(text:string):void,
    handleError(text:string):void
  }
) {
  const [gapiLoaded, setGapiLoaded] = useState(false);
  const [gisLoaded, setGisLoaded] = useState(false);
  const [tokenClient, setTokenClient] = useState<any>(null);
  const [showIntegration, setShowIntegration] = useState<boolean>(false);

  useEffect(() => {
    if(!gapiLoaded) {
      gapi.load('client', async () => {
        await gapi.client.init({
          apiKey: API_KEY,
          discoveryDocs: [DISCOVERY_DOC],
        });
        setGapiLoaded(true);
      });
    }

    if(!gisLoaded) {
      scriptLoader('https://accounts.google.com/gsi/client', () => {
        const client = window.google.accounts.oauth2.initTokenClient({
          client_id: CLIENT_ID,
          scope: SCOPES,
          callback: '', // Will be set later
        });
        setTokenClient(client);
        setGisLoaded(true);
      });
    }
  }, []);

  const enableIntegrationIfLoaded = () => {
    if (gapiLoaded && gisLoaded) {
      setShowIntegration(true)
    }
  };

  useEffect(() => {
    enableIntegrationIfLoaded();
  }, [gapiLoaded, gisLoaded]);

  const revokeToken = () => {
    const token = window.gapi.client.getToken();
    if (token !== null) {
      window.google.accounts.oauth2.revoke(token.access_token);
      gapi.client.setToken(null);
    }
  };

  const handleAuthClick = () => {
    tokenClient.callback = async (resp: any) => {
      try {
        await listMajors();
        revokeToken()
      } catch (error) {
        handleError('Could not authorize google account')
      }
    };

    if (window.gapi.client.getToken() === null) {
      tokenClient.requestAccessToken({ prompt: 'consent' });
    } else {
      tokenClient.requestAccessToken({ prompt: '' });
    }
  };

  const listMajors = async () => {
    let response;
    try {
      response = await gapi.client.sheets.spreadsheets.values.get({
        spreadsheetId: '1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms',
        range: 'Class Data!A1:E',
      });
      const value:string = response?.body;
      if (value) {
        handleSheets(value)
      }
    } catch (error: any) {
      handleError('Could not get google sheets')
      return;
    }
  };

  return (
    <div>
      {
        showIntegration &&
        <Integration 
          img={googleSheets.src}
          label="Google Sheets"
          alt="google sheets logo"
          onClickFunction={handleAuthClick}
        />
      }
    </div>
  );
}
