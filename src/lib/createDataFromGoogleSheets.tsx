export default function createDataFromGoogleSheets(data: Array<Array<String>>):Array<Object> {
  const headers = data[0]
  console.log(headers)
  if(data.length > 1) {
    const body = data.slice(1)
    console.log(body)
    const rows = body.map(item => {
      return headers.reduce((obj: any, header, index) => {
        obj[header as any] = item[index]
        return obj
      }, {})
    })
    return rows
  }
  return headers.reduce((obj:any, header, index) => {
    obj[header as any] = headers[index]
  }, {})
}