'use client'
import React from "react";
import Integration from "./integration";
import airtableImage from "@/assets/airtable.svg"
import Airtable from "airtable";

const accessToken = process.env.NEXT_PUBLIC_AIRTABLE_PERSONAL_ACCESS_TOKEN!;
const baseID = process.env.NEXT_PUBLIC_BASE_ID!;

export default function AirTable (
  {
    handleData,
    handleError
  } : {
    handleData(records: Array<Object>):void,
    handleError(error: string):void
  }
) {
  Airtable.configure({apiKey: accessToken})
  const base = Airtable.base(baseID);
  
  const authorize = async () => {
    try {
      const response = await base.table('Table 1').select().all()
      const results:Array<Object> = response.map(record => {
        return record.fields
      })
      handleData(results)
    } catch(error) {
      handleError('Could not get data from airtable')
    }
  }
  return (
    <Integration 
      img={airtableImage.src}
      label="Air Table"
      alt="airtable logo"
      onClickFunction={authorize}
    />
  )
}