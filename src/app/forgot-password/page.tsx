'use client'
import React, {useState} from "react";
import styles from "../../styles/registration.module.css";
import TextBox from "@/components/TextBox";
import Button from "@/components/Button";
import {resetEmail} from "../../lib/firebase/firebase-functions"
import _ from "lodash";
import RegistrationLayout from '../../components/layouts/registrationLayout'
import { isEmailValid } from "@/lib/isEmailValid";

export default function Login () {
  const [passwordSent, setPasswordSent] = useState<boolean>(false)
  const [emailValue, setEmailValue] = useState<string>("")
  const [passwordResetError, setPasswordResetErrorValue] = useState<string>("")
  const [emailTouched, setEmailTouched] = useState<boolean>(false);
  const [emailErrorValue, setEmailErrorValue] = useState<string>("")
  
  const validateEmail = (email: string) => {
    const isValid = isEmailValid(email)
    if(!isValid) {
      setEmailErrorValue('Please enter a valid email address.')
    } else {
      setEmailErrorValue("")
    }
  }

  const debounce = _.debounce(() => validateEmail(emailValue), 500);

  const changeEmail = (e: React.ChangeEvent<HTMLInputElement>) => {
    const email = e.target.value;
    setEmailValue(email);
    if(!emailTouched) {
      setEmailTouched(true);
    }
    
    if(emailTouched) {
      debounce();
    }
  }

  const sendPasswordLink = async () => {
   const emailValid = isEmailValid(emailValue)
   if (emailValid) {
    try {
      await resetEmail(emailValue)
      setPasswordSent(true) 
     } catch (error) {
      setPasswordResetErrorValue('Could not sent password reset link. Please try again.')
     }
   } else {
    setEmailErrorValue('Please enter a valid email address.')
   }
  }

  return (
    <RegistrationLayout>
      <div className={styles.narrowContainer}>
        <div className={styles.title}>
          <h1 className={styles.title}>{!passwordSent ? 'Forgot Your Password' : 'Check Your Inbox'}</h1>
          {!passwordSent && <h2>Enter your email address below and we'll get you back on track.</h2>}
          {
            passwordResetError &&
            <p className={styles.error}>
              {passwordResetError}
            </p>
          }
        </div>
        {
          passwordSent ?
          <>
            <p>If your email address was found in our system you will receive an email shortly.</p>
            <p>Click the link in the email in order to reset your password.</p>
          </>
          :
          <form id="forgotPasswordForm" className={styles.form}>
            <TextBox
            id="txtUserID"
            label="Email"
            type="email"
            value={emailValue}
            onChangeText={changeEmail}
            error={emailErrorValue}
          />
            <Button
              label="Request reset link"
              onClickFunction={sendPasswordLink}
            />
          </form>
        }
        <div className={styles.extra}>
          <a href="/login">Back to login</a>
        </div>
      </div>
    </RegistrationLayout>
  )
}