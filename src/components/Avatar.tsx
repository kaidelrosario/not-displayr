'use client'
import React from "react";
import styles from "@/styles/avatar.module.css";
import { User } from "@/types/user";

export default function Avatar(
  {
    user,
    onClickFunction
  }:{
    user: User
    onClickFunction:Function
  }
) {
  const getFirstLetter = (name: string) => {
    return Array.from(name)[0]
  }

  const handleClick = (event: any) => {
    onClickFunction()
  }

  return(
    <div className={styles.avatar} onClick={handleClick}>
      {user?.email && getFirstLetter(user?.email)}
    </div>
  )
}